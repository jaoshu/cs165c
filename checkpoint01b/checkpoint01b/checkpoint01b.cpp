/***********************************************************************
* Program:
*    Assignment: Checkpoint01b, C++ Basics
*    Brother Alvey, CS165
* Author:
*    Jamie Hurd
* Summary:
*    Summaries are not necessary for checkpoint assignments.
* ***********************************************************************/
#include <cstdlib>
#include <iostream>

/************************************************************************
* getSize
************************************************************************/
int getSize(int size)
{
   size = 0;
   std::cout << "Enter the size of the list: ";
   std::cin >> size;
   return size;
}

/************************************************************************
* getList
************************************************************************/
int getList(int checkArray[], int count)
{
   count = 0;
   int i = 0;
   for(i = 0; checkArray; i++)
   {
      std::cout << "Enter the number for index " << i << ": ";
      std::cin >> i;
      count++;
   }
   return count;
}

/************************************************************************
* displayMultiples
************************************************************************/
void displayMultiples(int checkArray[])
{
   std::cout << "The following are divisible by 3:" << std::endl;
   for(int i = 0; checkArray; i++)
   {
      if(checkArray[i] % 3 == 0)
      {
         std::cout << i << std::endl;
      }
   }
}

/************************************************************************
* driver
************************************************************************/
int main()
{
   int size;
   int * checkArray = new int[size];
   int temp = getSize(size);
}
